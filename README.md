# Project Lower Higher

_**(Ce repository est un clone du dernier commit du projet initialement développé avec un compte étudiant. Le projet initial est privé et partager avec d’autres étudiant)**_

## Principe

L'application est inspirée du jeu "lower higher". Deux villes sont présentées au joueur. Le joueur connait la population de la ville du haut, et doit deviner si la ville du bas à une population plus grande ou petite. À chaque bonne réponse le joueur gagne un point. La partie s'arrête lorsque le joueur se trompe.

Le score du joueur peut être sauvegardé. Le joueur courant (nom, photo et position) peut être édité sur une activité dédiée

## Plan

```mermaid
graph TD
	Main --> Highscore
	Main --> Gameplay
	Main --> User_form
	User_form --> Main
	Gameplay --> Endgame
	Endgame --> Gameplay
```

## Fonctionnalités implémentées

|fonctionnalité|activité|
|---|---|
|changement layout par orientation|User form
|différentes langues|Main
|Base de données locale|Gameplay
|Base de données distante|Endgame, Highscore
|Photographie|User form
|Géolocalisation|User form
|Fragment|Endgame

## Détail technique

### "Main Activity"

![Main Activity](_media/images/main.png)

Dans l'activité principale, nous avons des boutons menant à l'activité du jeu, (gameplay) du score (highscore) et de la page d'édition du joueur (user form).

#### préférences

Pour stocker le nom et ville du joueur, nous avons utilisé des préférences de l'application :

```java
private void intializeUser() {
        userPreferences = getSharedPreferences(USER_SHARED_PREFERENCES, 0);
        String fetchedUserName = userPreferences.getString(USER_SHARED_PREFERENCES_USER_NAME, null);
        String fetchedUserLocation =
                userPreferences.getString(USER_SHARED_PREFERENCES_USER_LOCATION, null);
        if (fetchedUserName != null && fetchedUserLocation != null) {
            setUser(new UserModel(fetchedUserName, fetchedUserLocation));
        }
    }

private void setUser(UserModel user) {
	this.user = user;
	userNameTextView.setText(user.getName());
	userLocationTextView.setText(user.getLocation());

	ContextWrapper contextWrapper = new ContextWrapper(getApplicationContext());
	File directory = contextWrapper.getDir(USER_PROFILE_PICTURE_DIRECTORY,
			Context.MODE_PRIVATE);
	File file = new File(directory, user.getName() + ".bmp");
	userProfileImageView.setImageDrawable(Drawable.createFromPath(file.toString()));
}
```
#### Traduction

Pour les éléments de cette activité, nous avons généré des traductions entre l'anglais, le français, l'espagnol et le chinois. Pour ce faire, nous avons mis nos textes dans des tags de la ressource `string.xml`. On crée ensuite d'autres fichiers `string.xml` dans des dossiers nommés en fonction du pays appliqué.

```text
.
├── values/
│   └── strings.xml
├── values-es/
│   └── strings.xml
├── values-fr/
│   └── strings.xml
└── values-zh-rCN/
    └── strings.xml
```

### "HighScore"

Cette activité liste les scores sauvegardés sur la base distante

![Highscore activity](_media/images/highscore.png)

#### Base de données

> Nous avons écrit en dure les élements d'accès de la base (dont mot de passe) pour plusieurs raisons:
>
> * La base de données utilisé provient d'un service gratuit, proposant des bases à taille limité, conservé pendant un mois. Le risque de réutilisation de la base est minime.
> * Les soruces de notre code ne sont pas posté publiquement
> * Nous n'allons pas conserver la base au dela du projet
>
> Nous savons que c'est une mauvaise pratique, mais nous avons pris ce racoruci par soucis de temps.

Pour la base de données distante, de la même manière que pour la base locale, nous avons struturé notre code autours de 'models' 'repository' et de 'connectionHanlers'.

```text
.
├── model/
│   └── ScoreModel.java
├── repository/
│   ├── ScoreRepository
│   └── ScoreRepositoryImpl
└── databases/
    └── ScoreDBConnectionHandler.java
```
`ScoreDBConnectionHandler` est une classe permettant de manipuler un singleton représentant la connection avec la base de données. La classe permet de récupérer et insérer des entités et est utilisé par les repositories (Les queries sont exécuté au niveau de ce singleton):

Les repositories interragissent avec l'handler, et fournissent les queries adapté à la méthode. l'implémentation du répository est l'objet de plus haut niveau utilisé pour intérragir avec la base:

```java
// niveau handler


// niveau repository

public List<ScoreModel> getTopScore(int count) {
        String x =
                "SELECT * FROM score ORDER BY " + ScoreModel.COLUMN_SCORE + " DESC LIMIT " + count;
        try {
            return scoreDBConnectionHandler.getEntities(x);

// niveau activité:

ScoreRepository scoreRepository = new ScoreRepositoryImpl();
scoreModels = new ArrayList(scoreRepository.getTopScore(100));
return scoreModels;
```

#### ListView

### "User Form"

Cette activié permet d'éditer le joueur courant.

![User form activity](_media/images/userform.png)

### Prise de photo

Pour prendre la photo de l'utilisateur, on utilise un intent de ``
```java
private void onPhotoButtonPressed(View view) {
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(cameraIntent, PIC_ID);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Match the request 'pic id with requestCode
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PIC_ID) {
            profilePicture = (Bitmap) data.getExtras().get("data");
            imageView.setImageBitmap(profilePicture);
        }
    }
```
### Géolocalisation

```java
@RequiresPermission(Manifest.permission.ACCESS_COARSE_LOCATION)
    private void onLocationButtonPressed(View view) {
        if (checkAndRequestPermissions()) {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0,
                    new LocationListener() {
                        @RequiresPermission(Manifest.permission.ACCESS_COARSE_LOCATION)
                        public void onLocationChanged(Location location) {
                            Log.i("Location Changed", location.toString());
                            if (location != null) {
                                Log.i("Location Changed",
                                        location.getLatitude() + " and " + location.getLongitude());
                                Double lat = location.getLatitude();
                                Double lng = location.getLongitude();

								...
		
```

Nous avons annoté en tant que position de l'utilisateur la ville étant au plus proche des coordonnéees lu. La ville est prise de la base de données locale, utiliser pour la phase de jeu:

```java
								...

								String closestCityName = "";
                                for (WorldCityModel city : worldCityModelList) {
                                    Double currentDistance = haversine(lat, lng,
                                            city.getLatitude(), city.getLongitude());
                                    if (currentDistance < minDistance) {
                                        minDistance = currentDistance;
                                        closestCityName = city.getName();
                                    }
                                }
                                user.setLocation(closestCityName);
                                locationText.setText(closestCityName);
```


### Enregistrement de l'utilisateur

Une fois que le formulaire est rempli, on enregistre les informations de l'utilisateur dans des préférences:

```java
private void onSaveButtonPressed(View view) {
	...

	SharedPreferences.Editor sharedPreferenceEditor = userPreferences.edit();
	sharedPreferenceEditor.putString(USER_SHARED_PREFERENCES_USER_NAME, user.getName());
	sharedPreferenceEditor.putString(USER_SHARED_PREFERENCES_USER_LOCATION,
			user.getLocation());
	sharedPreferenceEditor.apply();
```


### "Gameplay"

Cette activité est l'activité correspondant au jeu

![Gameplay activity](_media/images/gameplay.png)

#### Base de données locale

Pour la phase de jeu nous avions initialement fait des requêtes sur la base locale à chaque phase de jeu. Cependant ces requêtes était faites sur le thread peincipale.Cela posait problème car une requête longue bloquerai l'interface. Avant de changer de méthode nous avons fait néanmoins la réflexion suivante:

- Parce que la base de donnée est locale, et statique, (on n'effectue pas d'insertion) celle-ci est stable et il y a peu de chance qu'une query prend du temps ou échoue
- L'ensemble de la base est relativement petite, (± 10 000 lignes)

Nous avons donc appliqué la méthode suivante : 
Nous chargeons en ram (dans un singleton) l'intégralité de la table des villes au démarrage de l'application.

```java
protected Void doInBackground(Void... voids) {
	worldCitiesRepository = new WorldCitiesRepositoryRamImpl(MainActivity.this);
	return null;
}

public WorldCitiesRepositoryRamImpl(Context context){
	if (worldCities.isEmpty()) {
		worldCitiesDB = new WorldCitiesDB(context);
		sqLiteDatabase = worldCitiesDB.openDatabase();
		worldCities.addAll(cursorToList(sqLiteDatabase.rawQuery("SELECT * FROM worldcities", null)));
	}
}
```


### "Endgame"

'Endgame' est l'activité suivant la phase de jeu, présentant les résultats :

![End game activity](_media/images/endgame.png)

#### Fragment Google Map

On utilise un fragment pour afficher une carte des villes. L'activité supportant le fragment étend la classe `getSupportFragmentManager` qui permet notamment de gérer un fragment, et surtout son cycle de vie et ses callbacks

```java
SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);



@Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        Intent intent = getIntent();
        List<WorldCityModel> visitedCities = (List<WorldCityModel>) intent.getSerializableExtra(
                "visitedCities");

        listLatLng = new ArrayList<>();

        // Adds a marker for every visited city.
        for (int i = 0; i < visitedCities.size(); ++i) {
            WorldCityModel city = visitedCities.get(i);
            LatLng latLng = new LatLng(city.getLatitude(), city.getLongitude());
            mMap.addMarker(new MarkerOptions().position(latLng).title(city.getName()));
            listLatLng.add(latLng);
        }
```

#### Base de données distante

Pour envoyé des données vers la base de données distante, on utilise des repository, similaire aux interactions expliqué précédemment

```java

// Niveau activité
scoreRepository.insertScore(score);


// Niveau repository
public void insertScore(ScoreModel score) {
	String x =
			"INSERT INTO score (" + ScoreModel.COLUMN_USER_NAME + ", " + ScoreModel.COLUMN_SCORE + ", " + ScoreModel.COLUMN_DATE +
					") VALUES " + score.toValues();
	try {
		scoreDBConnectionHandler.updateEntity(x);


// Niveau handler
public void updateEntity(String query) throws SQLException {
	try {
		SCORE_DB_CONNECTION_INSTANCE.computeUpdate(query);