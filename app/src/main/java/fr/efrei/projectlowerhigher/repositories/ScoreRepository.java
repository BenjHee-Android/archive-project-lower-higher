package fr.efrei.projectlowerhigher.repositories;

import java.util.List;

import fr.efrei.projectlowerhigher.models.ScoreModel;

public interface ScoreRepository {
    List<ScoreModel> getAllScores();

    ScoreModel getScoreByUserName(String name);

    List<ScoreModel> getTopScore(int count);

    void insertScore (ScoreModel score);
}
