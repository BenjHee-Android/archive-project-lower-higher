package fr.efrei.projectlowerhigher.repositories;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import fr.efrei.projectlowerhigher.databases.WorldCitiesDB;
import fr.efrei.projectlowerhigher.models.WorldCityModel;

public class WorldCitiesRepositoryRamImpl implements WorldCitiesRepository {

    private WorldCitiesDB worldCitiesDB;
    private SQLiteDatabase sqLiteDatabase;

    private static final ArrayList<WorldCityModel> worldCities = new ArrayList<>();


    public WorldCitiesRepositoryRamImpl(Context context){
        if (worldCities.isEmpty()) {
            worldCitiesDB = new WorldCitiesDB(context);
            sqLiteDatabase = worldCitiesDB.openDatabase();
            worldCities.addAll(cursorToList(sqLiteDatabase.rawQuery("SELECT * FROM worldcities", null)));
        }
    }

    private List<WorldCityModel> cursorToList(Cursor cursor) {
        return cursor.getCount() > 0 ? WorldCityModel.worldCitesFromCursor(cursor) : Collections.emptyList();
    }

    @Override
    public List<WorldCityModel> getAllWorldCities() {
        return worldCities;
    }

    @Override
    public WorldCityModel getWorldCityByName(String name) {
        for (WorldCityModel city : worldCities) {
            if (city.getName().equals(name)) {
                return city;
            }
        }
        return null;
    }

    @Override
    public List<WorldCityModel> getWorldCityByCountries(String country) {
        List<WorldCityModel> worldCityModels = new ArrayList<>();
        for (WorldCityModel city : worldCities) {
            if (city.getCountry().equals(country)) {
                worldCityModels.add(city);
            }
        }
        return worldCityModels;
    }

    @Override
    public List<WorldCityModel> getWorldCitiesByPopulationRange(Double min, Double max) {
        List<WorldCityModel> worldCityModels = new ArrayList<>();
        for (WorldCityModel city : worldCities) {
            if (city.getPopulation() >= min && city.getPopulation() <= max) {
                worldCityModels.add(city);
            }
        }
        return worldCityModels;
    }
}
