package fr.efrei.projectlowerhigher.repositories;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import fr.efrei.projectlowerhigher.databases.WorldCitiesDB;
import fr.efrei.projectlowerhigher.models.WorldCityModel;

public class WorldCitiesRepositoryDbFileImpl implements WorldCitiesRepository {

    private final WorldCitiesDB worldCitiesDB;
    private final SQLiteDatabase sqLiteDatabase;

    private List<WorldCityModel> cursorToList(Cursor cursor) {
        return cursor.getCount() > 0 ? WorldCityModel.worldCitesFromCursor(cursor) : Collections.emptyList();
    }

    private WorldCityModel cursorToModel(Cursor cursor) {
        if (cursor.getCount() > 0) {
            cursor.moveToNext();
            return new WorldCityModel(cursor);
        } else {
            return null;
        }
    }

    public WorldCitiesRepositoryDbFileImpl(Context context) {
        worldCitiesDB = new WorldCitiesDB(context);
        sqLiteDatabase = worldCitiesDB.openDatabase();
    }

    @Override
    public List<WorldCityModel> getAllWorldCities() {
        return cursorToList(sqLiteDatabase.rawQuery("SELECT * FROM worldcities", null));

    }

    @Override
    public WorldCityModel getWorldCityByName(String name) {
        return cursorToModel(sqLiteDatabase.rawQuery("SELECT * FROM worldcities WHERE name = '" + name + "' ORDER BY population DESC LIMIT 1", null));
    }

    @Override
    public List<WorldCityModel> getWorldCityByCountries(String country) {
        return cursorToList(sqLiteDatabase.rawQuery("SELECT * FROM worldcities WHERE country = '" + country + "'", null));
    }

    @Override
    public List<WorldCityModel> getWorldCitiesByPopulationRange(Double min, Double max) {
        return cursorToList(sqLiteDatabase.rawQuery("SELECT * FROM worldcities WHERE population BETWEEN " + min + " AND " + max, null));
    }
}
