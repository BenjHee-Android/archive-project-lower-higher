package fr.efrei.projectlowerhigher.repositories;

import java.util.List;

import fr.efrei.projectlowerhigher.models.WorldCityModel;

public interface WorldCitiesRepository {
    List<WorldCityModel> getAllWorldCities();

    WorldCityModel getWorldCityByName(String name);

    List<WorldCityModel> getWorldCityByCountries(String country);

    List<WorldCityModel> getWorldCitiesByPopulationRange(Double min, Double max);
}
