package fr.efrei.projectlowerhigher.repositories;

import android.util.Log;

import java.sql.SQLException;
import java.util.Collections;
import java.util.List;

import fr.efrei.projectlowerhigher.databases.ScoreDBConnectionHandler;
import fr.efrei.projectlowerhigher.models.ScoreModel;

public class ScoreRepositoryImpl implements ScoreRepository {

    private final ScoreDBConnectionHandler scoreDBConnectionHandler;

    public ScoreRepositoryImpl() {
        scoreDBConnectionHandler = new ScoreDBConnectionHandler();
    }

    @Override
    public List<ScoreModel> getAllScores() {
        String x = "SELECT * FROM score";
        try {
            return scoreDBConnectionHandler.getEntities(x);
        } catch (SQLException e) {
            Log.e("SCORE_REPOSITORY", e.getMessage());
            return Collections.emptyList();
        }
    }

    @Override
    public ScoreModel getScoreByUserName(String name) {
        return null;
    }

    @Override
    public List<ScoreModel> getTopScore(int count) {
        String x =
                "SELECT * FROM score ORDER BY " + ScoreModel.COLUMN_SCORE + " DESC LIMIT " + count;
        try {
            return scoreDBConnectionHandler.getEntities(x);
        } catch (SQLException e) {
            Log.e("SCORE_REPOSITORY", e.getMessage());
            return Collections.emptyList();
        }
    }

    @Override
    public void insertScore(ScoreModel score) {
        String x =
                "INSERT INTO score (" + ScoreModel.COLUMN_USER_NAME + ", " + ScoreModel.COLUMN_SCORE + ", " + ScoreModel.COLUMN_DATE +
                        ") VALUES " + score.toValues();
        try {
            scoreDBConnectionHandler.updateEntity(x);
        } catch (SQLException e) {
            Log.e("SCORE_REPOSITORY", e.getMessage());
        }
    }
}
