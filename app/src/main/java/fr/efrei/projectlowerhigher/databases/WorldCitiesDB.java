package fr.efrei.projectlowerhigher.databases;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import fr.efrei.projectlowerhigher.BuildConfig;

public class WorldCitiesDB {
    private final int BUFFER_SIZE = 400000;

    private static final String LOG_TAG = WorldCitiesDB.class.getSimpleName();

    public static final String PACKAGE_NAME = BuildConfig.APPLICATION_ID;// The package name of the app
    public static final String DB_BANE = "worldcities.sqlite";// Name of the saved database file
    public static final String DB_PATH = "/data" + Environment.getDataDirectory().getAbsolutePath() + "/"
            + PACKAGE_NAME + "/databases";// Where to store the database in your mobile phone
    private final Context context;// The context of the component

    public WorldCitiesDB(Context context) {

        this.context = context;
    }

    public SQLiteDatabase openDatabase() {

        try {

            File myDataPath = new File(DB_PATH);
            if (!myDataPath.exists()) {

                myDataPath.mkdirs();
            }
            String dbFile = myDataPath + "/" + DB_BANE;
            if (!(new File(dbFile).exists())) {

                InputStream is = context.getAssets().open(DB_BANE);
                //InputStream is = context.getResources().openRawResource(R.raw.nations);
                FileOutputStream fos = new FileOutputStream(dbFile);
                byte[] buffer = new byte[BUFFER_SIZE];
                int count = 0;
                while ((count = is.read(buffer)) != -1) {

                    Log.e(LOG_TAG, " write in " + String.valueOf(count));
                    fos.write(buffer, 0, count);
                }
                fos.flush();
                fos.close();
                is.close();
            }

            SQLiteDatabase db = SQLiteDatabase.openOrCreateDatabase(dbFile, null);
            return db;
        } catch (FileNotFoundException e) {

            Log.e(LOG_TAG, "File not found!");
            e.printStackTrace();
        } catch (IOException e) {

            Log.e(LOG_TAG, "IO exception");
            e.printStackTrace();
        }
        return null;
    }

}
