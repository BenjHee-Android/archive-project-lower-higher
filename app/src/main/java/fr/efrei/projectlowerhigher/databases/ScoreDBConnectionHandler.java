package fr.efrei.projectlowerhigher.databases;

import android.util.Log;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import fr.efrei.projectlowerhigher.models.ScoreModel;

class ScoreDbConnectionInstance {


    private static final String URL = "jdbc:mysql://bl5kntrkua9lzxsoxcbn-mysql.services" +
            ".clever-cloud.com:3306/bl5kntrkua9lzxsoxcbn";
    private static final String USER = "u5gelz9ujrdtpzkc";
    private static final String PASSWORD = "LtGTnPESGVrmWmQpDBY2";

    Connection connection;

    ScoreDbConnectionInstance() {
    }

    public ResultSet computeStatement(String query) throws SQLException {
        try {
            connection = DriverManager.getConnection(URL, USER, PASSWORD);
            return connection.prepareStatement(query).executeQuery();
        } catch (SQLException e) {
            Log.e("SCOREDB_STATEMENT", e.getMessage());
        }
        return null;
    }

    public int computeUpdate(String query) throws SQLException {
        connection = DriverManager.getConnection(URL, USER, PASSWORD);
        return connection.prepareStatement(query).executeUpdate();
    }

    public void closeConnection() throws SQLException {
        if (!connection.isClosed()) {
            connection.close();
        }
    }
}

public class ScoreDBConnectionHandler {

    private static ScoreDbConnectionInstance SCORE_DB_CONNECTION_INSTANCE = null;

    public ScoreDBConnectionHandler() {
        if (SCORE_DB_CONNECTION_INSTANCE == null) {
            SCORE_DB_CONNECTION_INSTANCE = new ScoreDbConnectionInstance();
        }
    }

    public List<ScoreModel> getEntities(String query) throws SQLException {
        try {
            ResultSet resultSet = SCORE_DB_CONNECTION_INSTANCE.computeStatement(query);
            List<ScoreModel> scoreModels = new ArrayList<>();
            do {
                resultSet.next();
                scoreModels.add(new ScoreModel(resultSet));
            } while (!resultSet.isLast());
            return scoreModels;
        } catch (SQLException e) {
            Log.e("SCOREDB_HANDLER", e.getMessage());
        } finally {
            SCORE_DB_CONNECTION_INSTANCE.closeConnection();
        }
        return Collections.emptyList();
    }

    public void updateEntity(String query) throws SQLException {
        try {
            SCORE_DB_CONNECTION_INSTANCE.computeUpdate(query);
        } catch (SQLException e) {
            Log.e("SCOREDB_HANDLER", e.getMessage());
        } finally {
            SCORE_DB_CONNECTION_INSTANCE.closeConnection();
        }
    }
}
