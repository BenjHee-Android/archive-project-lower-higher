package fr.efrei.projectlowerhigher;

public class Constants {
    public static final String USER_SHARED_PREFERENCES = "USER";
    public static final String USER_SHARED_PREFERENCES_USER_NAME = "USER_NAME";
    public static final String USER_SHARED_PREFERENCES_USER_LOCATION = "USER_LOCATION";
    public  static final String USER_PROFILE_PICTURE_DIRECTORY = "userProfilePictureDir";
}
