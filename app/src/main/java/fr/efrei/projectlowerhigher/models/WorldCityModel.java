package fr.efrei.projectlowerhigher.models;


import android.database.Cursor;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class WorldCityModel implements Serializable {
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_LAT = "lat";
    public static final String COLUMN_LON = "lon";
    public static final String COLUMN_COUNTRY = "country";
    public static final String COLUMN_POPULATION = "population";

    private final Integer id;
    private final String name;
    private final Integer population;
    private final String country;
    private final Double latitude;
    private final Double longitude;

    public WorldCityModel(Integer id, String name, Integer population, String country, Double latitude, Double longitude) {
        this.id = id;
        this.name = name;
        this.population = population;
        this.country = country;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public WorldCityModel(Cursor cursor) {
        this.id = cursor.getInt(cursor.getColumnIndexOrThrow(COLUMN_ID));
        this.name = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_NAME));
        this.population = cursor.getInt(cursor.getColumnIndexOrThrow(COLUMN_POPULATION));
        this.latitude = cursor.getDouble(cursor.getColumnIndexOrThrow(COLUMN_LAT));
        this.longitude = cursor.getDouble(cursor.getColumnIndexOrThrow(COLUMN_LON));
        this.country = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_COUNTRY));
    }

    public static List<WorldCityModel> worldCitesFromCursor(Cursor cursor) {
        List<WorldCityModel> output = new ArrayList();
        while (cursor.moveToNext()) {
            output.add(new WorldCityModel(cursor));
        }
        return output;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Integer getPopulation() {
        return population;
    }

    public String getCountry() {
        return country;
    }

    public Double getLatitude() {
        return latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    @Override
    public String toString() {
        return "WorldCityModel{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", population=" + population +
                ", country='" + country + '\'' +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                '}';
    }
}
