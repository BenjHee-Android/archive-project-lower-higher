package fr.efrei.projectlowerhigher.models;

public class UserModel {
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    private String name;
    private String location;

    public UserModel(String name, String location) {
        this.name = name;
        this.location = location;
    }
    public UserModel(){
        location = "EMPTY";
    }
}
