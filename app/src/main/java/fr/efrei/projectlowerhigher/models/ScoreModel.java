package fr.efrei.projectlowerhigher.models;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ScoreModel {

    public static final String COLUMN_USER_NAME = "userName";
    public static final String COLUMN_SCORE = "score";
    public static final String COLUMN_DATE = "date";

    public ScoreModel(ResultSet resultSet) throws SQLException {
        userName = resultSet.getString(COLUMN_USER_NAME);
        score = resultSet.getInt(COLUMN_SCORE);
        date = resultSet.getTimestamp(COLUMN_DATE);
    }

    public ScoreModel() {}

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public ScoreModel(String userName, Integer score) {
        this.userName = userName;
        this.score = score;
    }

    @Override
    public String toString() {
        return "ScoreModel{" +
                "userName='" + userName + '\'' +
                ", score=" + score +
                ", date=" + date +
                '}';
    }

    // Used for update statement.
    public String toValues() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        return "('" + userName + "', " + score + ", '" + simpleDateFormat.format(date) + "')";
    }

    private String userName;
    private Integer score;
    private Date date;
}
