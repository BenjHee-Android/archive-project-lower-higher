package fr.efrei.projectlowerhigher.activities;

import static fr.efrei.projectlowerhigher.Constants.USER_SHARED_PREFERENCES;
import static fr.efrei.projectlowerhigher.Constants.USER_SHARED_PREFERENCES_USER_NAME;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.FragmentActivity;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import fr.efrei.projectlowerhigher.R;
import fr.efrei.projectlowerhigher.databinding.ActivityEndgameBinding;
import fr.efrei.projectlowerhigher.models.ScoreModel;
import fr.efrei.projectlowerhigher.models.WorldCityModel;
import fr.efrei.projectlowerhigher.repositories.ScoreRepository;
import fr.efrei.projectlowerhigher.repositories.ScoreRepositoryImpl;

public class EndgameActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private ActivityEndgameBinding binding;
    private TextView finalScoreTextView;
    private Button retryButton;
    private Button uploadScoreButton;
    private Button previousButton;
    private Button nextButton;
    private ArrayList<LatLng> listLatLng;
    private int currentLatLngIndex;
    private int finalScore;
    private ScoreRepository scoreRepository;
    SharedPreferences userPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityEndgameBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        userPreferences = getSharedPreferences(USER_SHARED_PREFERENCES, 0);
        finalScore = getIntent().getIntExtra("score", 0);
        scoreRepository = new ScoreRepositoryImpl();

        initializeView();
    }

    private class DBTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... voids) {
            ScoreModel score = new ScoreModel();
            score.setUserName(userPreferences.getString(USER_SHARED_PREFERENCES_USER_NAME, null));
            score.setScore(finalScore);
            score.setDate(new Date());
            scoreRepository.insertScore(score);
            return null;
        }

        @Override
        protected void onPostExecute(Void unused) {
            Toast.makeText(EndgameActivity.this, String.format(Locale.FRANCE, "Score uploaded" +
                            ". [Score = %d]",
                    finalScore), Toast.LENGTH_SHORT).show();
            super.onPostExecute(unused);
        }
    }

    private void initializeView() {
        retryButton = findViewById(R.id.EndgameActivity_retryButton);
        retryButton.setOnClickListener(this::onRetryButtonPressed);
        uploadScoreButton = findViewById(R.id.EndgameActivity_uploadScoreButton);
        uploadScoreButton.setOnClickListener(this::onUploadScoreButtonPressed);
        previousButton = findViewById(R.id.EndgameActivity_previousButton);
        previousButton.setOnClickListener(this::onPreviousButtonPressed);
        nextButton = findViewById(R.id.EndgameActivity_nextButton);
        nextButton.setOnClickListener(this::onNextButtonPressed);
        finalScoreTextView = findViewById(R.id.EndgameActivity_finalScoreText);
        finalScoreTextView.setText(String.format(Locale.FRANCE, "%d", finalScore));
    }

    private void onRetryButtonPressed(View view) {
        finish();
    }

    private void onUploadScoreButtonPressed(View view) {
        DBTask dbTask = new DBTask();
        dbTask.execute();
        uploadScoreButton.setEnabled(false);
    }

    private void onPreviousButtonPressed(View view) {
        currentLatLngIndex = (currentLatLngIndex + listLatLng.size() - 1) % listLatLng.size();
        mMap.animateCamera(
                CameraUpdateFactory.newLatLng(listLatLng.get(currentLatLngIndex)));
    }

    private void onNextButtonPressed(View view) {
        currentLatLngIndex = (currentLatLngIndex + 1) % listLatLng.size();
        mMap.animateCamera(
                CameraUpdateFactory.newLatLng(listLatLng.get(currentLatLngIndex)));
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        Intent intent = getIntent();
        List<WorldCityModel> visitedCities = (List<WorldCityModel>) intent.getSerializableExtra(
                "visitedCities");

        listLatLng = new ArrayList<>();

        // Adds a marker for every visited city.
        for (int i = 0; i < visitedCities.size(); ++i) {
            WorldCityModel city = visitedCities.get(i);
            LatLng latLng = new LatLng(city.getLatitude(), city.getLongitude());
            mMap.addMarker(new MarkerOptions().position(latLng).title(city.getName()));
            listLatLng.add(latLng);
        }

        // Adds a polyline that goes through every visited city.
        mMap.addPolyline((new PolylineOptions()).addAll(listLatLng)
                .width(5)
                .color(Color.RED)
                .geodesic(true));

        currentLatLngIndex = listLatLng.size() - 1;

        // Moves the camera to the last city.
        mMap.moveCamera(
                CameraUpdateFactory.newLatLngZoom(listLatLng.get(currentLatLngIndex), 5));
    }
}