package fr.efrei.projectlowerhigher.activities;

import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import fr.efrei.projectlowerhigher.R;
import fr.efrei.projectlowerhigher.activities.adapter.ScoreListAdapter;
import fr.efrei.projectlowerhigher.models.ScoreModel;
import fr.efrei.projectlowerhigher.repositories.ScoreRepository;
import fr.efrei.projectlowerhigher.repositories.ScoreRepositoryImpl;

public class HighscoreActivity extends AppCompatActivity {

    private TextView testTextView;
    private ListView scoresListView;

    private List<ScoreModel> scoreModels = new ArrayList<>();

    private class DBTask extends AsyncTask<Void,Void, List<ScoreModel>> {
        @Override
        protected List<ScoreModel> doInBackground(Void... voids) {
            try {
                ScoreRepository scoreRepository = new ScoreRepositoryImpl();
                scoreModels = new ArrayList(scoreRepository.getTopScore(100));
                return scoreModels;
            } catch (Exception e) {
                return Collections.emptyList();
            }
        }

        @Override
        protected void onPostExecute(List<ScoreModel> scoreModels) {
            if (!scoreModels.isEmpty()){
                testTextView.setText(scoreModels.toString());
                initListView();
            }
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_highscore);
        testTextView = findViewById(R.id.highscoreActivity_nameTextView);
        scoresListView = findViewById(R.id.highscoreActivity_scoreListView);
        DBTask dbTask = new DBTask();
        dbTask.execute();
    }

    private void initListView(){
        scoresListView.setAdapter(new ScoreListAdapter(this,scoreModels));
    }

}
