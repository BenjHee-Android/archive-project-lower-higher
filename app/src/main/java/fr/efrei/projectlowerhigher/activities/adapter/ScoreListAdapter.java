package fr.efrei.projectlowerhigher.activities.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;
import java.util.Locale;

import fr.efrei.projectlowerhigher.R;
import fr.efrei.projectlowerhigher.models.ScoreModel;

public class ScoreListAdapter extends BaseAdapter {
    private List<ScoreModel> scoreModelList;
    private LayoutInflater layoutInflater;

    public ScoreListAdapter(Context context, List<ScoreModel> scoreModelList) {
        this.scoreModelList = scoreModelList;
        this.layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return scoreModelList.size();
    }

    @Override
    public Object getItem(int i) {
        return scoreModelList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder holder;
        if (view == null) {
            view = layoutInflater.inflate(R.layout.list_scores, null);
            holder = new ViewHolder();
            holder.uPlacement = (TextView) view.findViewById(R.id.list_score_placementTextView);
            holder.uUserName = (TextView) view.findViewById(R.id.list_score_userNameTextView);
            holder.uScore = (TextView) view.findViewById(R.id.list_score_scoreTextView);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        holder.uPlacement.setText("#"+ (i + 1));
        holder.uUserName.setText(scoreModelList.get(i).getUserName());
        holder.uScore.setText(scoreModelList.get(i).getScore().toString());
        return view;
    }

    static class ViewHolder {
        TextView uPlacement;
        TextView uUserName;
        TextView uScore;
    }
}
