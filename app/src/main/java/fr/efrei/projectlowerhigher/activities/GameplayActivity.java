package fr.efrei.projectlowerhigher.activities;

import static fr.efrei.projectlowerhigher.Constants.USER_SHARED_PREFERENCES;
import static fr.efrei.projectlowerhigher.Constants.USER_SHARED_PREFERENCES_USER_LOCATION;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.Serializable;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Random;

import fr.efrei.projectlowerhigher.R;
import fr.efrei.projectlowerhigher.models.WorldCityModel;
import fr.efrei.projectlowerhigher.repositories.WorldCitiesRepository;
import fr.efrei.projectlowerhigher.repositories.WorldCitiesRepositoryRamImpl;

public class GameplayActivity extends AppCompatActivity {

    private TextView scoreTextView;
    private TextView topCountryNameTextView;
    private TextView topCityNameTextView;
    private TextView topCityPopulationTextView;
    private TextView bottomCountryNameTextView;
    private TextView bottomCityNameTextView;
    private TextView bottomCityPopulationTextView;
    private Button higherButton;
    private Button lowerButton;

    private WorldCityModel topCity;
    private WorldCityModel bottomCity;
    private Integer score;
    private final List<WorldCityModel> alreadyPlayedCities = new ArrayList<>();
    private double populationRangeFactor = DEFAULT_POPULATION_RANGE_FACTOR;

    SharedPreferences userPreferences;

    enum EndgameCode {
        FAILURE,
        NO_CITY_LEFT
    }

    private WorldCitiesRepository worldCitiesRepository;
    private final Random random = new Random();

    private static final double DEFAULT_POPULATION_RANGE_FACTOR = 0.50;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gameplay);
        initializeView();
        initializeGameplay();
        setGameplayViews();
    }

    private void setTopCity(WorldCityModel city) {
        topCity = city;
    }

    private void setTopCity(String name) {
        topCity = worldCitiesRepository.getWorldCityByName(name);
    }

    private void setBottomCity() {
        List<WorldCityModel> potentialBottomCity =
                worldCitiesRepository.getWorldCitiesByPopulationRange(0.0, Double.MAX_VALUE);
        potentialBottomCity.removeAll(alreadyPlayedCities);
        if (potentialBottomCity.isEmpty()) {
            endgame(EndgameCode.NO_CITY_LEFT);
        }
        bottomCity = potentialBottomCity.get(random.nextInt(potentialBottomCity.size()));
    }

    private void endgame(EndgameCode code) {
        switch (code) {
            case NO_CITY_LEFT:
                Toast.makeText(this, String.format(Locale.FRANCE, "GAME HALTED: No more city to " +
                        "play with. [Score = %d]", score), Toast.LENGTH_SHORT).show();
                break;
            case FAILURE:
                Toast.makeText(this, String.format(Locale.FRANCE, "GAME STOPPED: wrong answer " +
                        "[Score = %d]", score), Toast.LENGTH_SHORT).show();
                break;
            default:
                Toast.makeText(this, String.format(Locale.FRANCE, "GAME STOPPED: UNKNOWN " +
                        "INTERRUPTION[Score = %d]", score), Toast.LENGTH_LONG).show();
                break;
        }

        Intent endgameIntent = new Intent(this, EndgameActivity.class);
        endgameIntent.putExtra("score", score);
        endgameIntent.putExtra("visitedCities", (Serializable) alreadyPlayedCities);

        startActivity(endgameIntent);
        initializeGameplay();
    }

    private void initializeGameplay() {
        userPreferences = getSharedPreferences(USER_SHARED_PREFERENCES, 0);

        alreadyPlayedCities.clear();
        worldCitiesRepository = new WorldCitiesRepositoryRamImpl(this);
        setTopCity(userPreferences.getString(USER_SHARED_PREFERENCES_USER_LOCATION,"Paris"));
        alreadyPlayedCities.add(topCity);
        setBottomCity();
        score = 0;
    }

    private void setGameplayViews() {
        topCountryNameTextView.setText(topCity.getCountry());
        topCityNameTextView.setText(topCity.getName());
        topCityPopulationTextView.setText(NumberFormat.getNumberInstance(Locale.FRANCE).format(topCity.getPopulation()));
        bottomCountryNameTextView.setText(bottomCity.getCountry());
        bottomCityNameTextView.setText(bottomCity.getName());
        bottomCityPopulationTextView.setText("");
        scoreTextView.setText(String.format(Locale.FRANCE, "%d", score));
        higherButton.setVisibility(View.VISIBLE);
        lowerButton.setVisibility(View.VISIBLE);
    }

    private void initializeView() {
        scoreTextView = findViewById(R.id.GameplayActivity_scoreTextView);
        topCountryNameTextView = findViewById(R.id.GameplayActivity_topCountryNameTextView);
        topCityNameTextView = findViewById(R.id.GameplayActivity_topCityNameTextView);
        topCityPopulationTextView = findViewById(R.id.GameplayActivity_topCityPopulationTextView);
        bottomCountryNameTextView = findViewById(R.id.GameplayActivity_bottomCountryNameTextView);
        bottomCityNameTextView = findViewById(R.id.GameplayActivity_bottomCityNameTextView);
        bottomCityPopulationTextView =
                findViewById(R.id.GameplayActivity_bottomCityPopulationTextView);
        higherButton = findViewById(R.id.GameplayActivity_moreButton);
        higherButton.setOnClickListener(this::onHigherButtonPressed);
        lowerButton = findViewById(R.id.GameplayActivity_lessButton);
        lowerButton.setOnClickListener(this::onLowerButtonPressed);
    }

    private void onHigherButtonPressed(View view) {
        play(true);
    }

    private void onLowerButtonPressed(View view) {
        play(false);
    }

    private void play(boolean isHigherDecision) {

        float topCountryY = topCountryNameTextView.getY();
        float topCityY = topCityNameTextView.getY();
        float topPopulationY = topCityPopulationTextView.getY();

        float bottomCountryY = bottomCountryNameTextView.getY();
        float bottomCityY = bottomCityNameTextView.getY();
        float bottomPopulationY = bottomCityPopulationTextView.getY();

        // Moves these views up by half the screen's height using the divider's center Y.
        float travelDistance =
                -(findViewById(R.id.GameplayActivity_divider).getY() + findViewById(R.id.GameplayActivity_divider).getHeight());

        ObjectAnimator bottomCountryAnimation =
                ObjectAnimator.ofFloat(bottomCountryNameTextView,
                        bottomCountryNameTextView.TRANSLATION_Y, travelDistance);

        ObjectAnimator bottomCityAnimation =
                ObjectAnimator.ofFloat(bottomCityNameTextView,
                        bottomCityNameTextView.TRANSLATION_Y, travelDistance);

        ObjectAnimator bottomPopulationAnimation =
                ObjectAnimator.ofFloat(bottomCityPopulationTextView,
                        bottomCityPopulationTextView.TRANSLATION_Y, travelDistance);

        ObjectAnimator topCountryAnimation =
                ObjectAnimator.ofFloat(topCountryNameTextView,
                        topCountryNameTextView.TRANSLATION_Y, travelDistance);

        ObjectAnimator topCityAnimation =
                ObjectAnimator.ofFloat(topCityNameTextView,
                        topCityNameTextView.TRANSLATION_Y, travelDistance);

        ObjectAnimator topPopulationAnimation =
                ObjectAnimator.ofFloat(topCityPopulationTextView,
                        topCityPopulationTextView.TRANSLATION_Y, travelDistance);

        AnimatorSet scrollAnimationSet = new AnimatorSet();
        scrollAnimationSet.setDuration(1000);
        scrollAnimationSet.playTogether(bottomCountryAnimation, bottomCityAnimation,
                bottomPopulationAnimation, topCountryAnimation, topCityAnimation,
                topPopulationAnimation);

        scrollAnimationSet.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animator) {
                topCountryNameTextView.setY(topCountryY);
                topCityNameTextView.setY(topCityY);
                topCityPopulationTextView.setY(topPopulationY);
                bottomCountryNameTextView.setY(bottomCountryY);
                bottomCityNameTextView.setY(bottomCityY);
                bottomCityPopulationTextView.setY(bottomPopulationY);
                score++;
                setTopCity(bottomCity);
                setBottomCity();
                setGameplayViews();
            }
        });

        ValueAnimator bottomCityPopulationAnimation =
                ValueAnimator.ofInt(0, bottomCity.getPopulation()).setDuration(1000);
        bottomCityPopulationAnimation.addUpdateListener(animator -> {
            int value = (int) animator.getAnimatedValue();
            bottomCityPopulationTextView.setText(
                    NumberFormat.getNumberInstance(Locale.FRANCE).format(value));
        });

        bottomCityPopulationAnimation.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animator) {
                alreadyPlayedCities.add(bottomCity);
                boolean isEqual = bottomCity.getPopulation().equals(topCity.getPopulation());
                boolean isReallyHigher = bottomCity.getPopulation() > topCity.getPopulation();
                if (isEqual || (isHigherDecision == isReallyHigher)) {
                    scrollAnimationSet.start();
                } else {
                    endgame(EndgameCode.FAILURE);
                    setGameplayViews();
                }
            }
        });
        higherButton.setVisibility(View.INVISIBLE);
        lowerButton.setVisibility(View.INVISIBLE);
        bottomCityPopulationAnimation.start();
    }
}