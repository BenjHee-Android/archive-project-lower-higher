package fr.efrei.projectlowerhigher.activities;

import static fr.efrei.projectlowerhigher.Constants.USER_PROFILE_PICTURE_DIRECTORY;
import static fr.efrei.projectlowerhigher.Constants.USER_SHARED_PREFERENCES;
import static fr.efrei.projectlowerhigher.Constants.USER_SHARED_PREFERENCES_USER_LOCATION;
import static fr.efrei.projectlowerhigher.Constants.USER_SHARED_PREFERENCES_USER_NAME;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.io.File;

import fr.efrei.projectlowerhigher.R;
import fr.efrei.projectlowerhigher.models.UserModel;
import fr.efrei.projectlowerhigher.repositories.WorldCitiesRepository;
import fr.efrei.projectlowerhigher.repositories.WorldCitiesRepositoryRamImpl;

public class MainActivity extends AppCompatActivity {

    private Button playButton;
    private Button highScoreButton;
    private Button editUserButton;

    private ImageView userProfileImageView;
    private TextView userNameTextView;
    private TextView userLocationTextView;

    SharedPreferences userPreferences;

    WorldCitiesRepository worldCitiesRepository;

    private UserModel user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initializeWidgets();
        initializeDB();
        intializeUser();
    }

    @Override
    protected void onResume() {
        super.onResume();
        intializeUser();
    }

    private void intializeUser() {
        userPreferences = getSharedPreferences(USER_SHARED_PREFERENCES, 0);
        String fetchedUserName = userPreferences.getString(USER_SHARED_PREFERENCES_USER_NAME, null);
        String fetchedUserLocation =
                userPreferences.getString(USER_SHARED_PREFERENCES_USER_LOCATION, null);
        if (fetchedUserName != null && fetchedUserLocation != null) {
            setUser(new UserModel(fetchedUserName, fetchedUserLocation));
        }
    }

    private void setUser(UserModel user) {
        this.user = user;
        userNameTextView.setText(user.getName());
        userLocationTextView.setText(user.getLocation());

        ContextWrapper contextWrapper = new ContextWrapper(getApplicationContext());
        File directory = contextWrapper.getDir(USER_PROFILE_PICTURE_DIRECTORY,
                Context.MODE_PRIVATE);
        File file = new File(directory, user.getName() + ".bmp");
        userProfileImageView.setImageDrawable(Drawable.createFromPath(file.toString()));
    }

    private class DBTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... voids) {
            worldCitiesRepository = new WorldCitiesRepositoryRamImpl(MainActivity.this);
            return null;
        }

        @Override
        protected void onPostExecute(Void unused) {
            if (worldCitiesRepository.getAllWorldCities().isEmpty()) {
                Toast.makeText(MainActivity.this, "Error occurred when loading data", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(MainActivity.this, "Data loaded", Toast.LENGTH_SHORT).show();
                playButton.setEnabled(true);
                editUserButton.setEnabled(true);
            }
            super.onPostExecute(unused);
        }
    }

    private void initializeDB() {
        DBTask dbTask = new DBTask();
        dbTask.execute();
    }

    private void initializeWidgets() {
        playButton = findViewById(R.id.mainActivity_playButton);
        playButton.setOnClickListener(this::onPlayButtonPressed);
        highScoreButton = findViewById(R.id.mainActivity_highscoreButton);
        highScoreButton.setOnClickListener(this::onHighscoreButtonPressed);
        editUserButton = findViewById(R.id.mainActivity_editUserButton);
        editUserButton.setOnClickListener(this::onEditUserButtonPressed);
        userProfileImageView = findViewById(R.id.mainActivity_userPictureImageView);
        userNameTextView = findViewById(R.id.mainActivity_userNameTextView);
        userLocationTextView = findViewById(R.id.mainActivity_userLocationTextView);

        playButton.setEnabled(false);
        editUserButton.setEnabled(false);
    }

    private void onEditUserButtonPressed(View view) {
        startActivity(new Intent(this, UserFormActivity.class));
    }

    private void onPlayButtonPressed(View view) {
        startActivity(new Intent(this, GameplayActivity.class));
    }

    private void onHighscoreButtonPressed(View view) {
        startActivity(new Intent(this, HighscoreActivity.class));
    }
}