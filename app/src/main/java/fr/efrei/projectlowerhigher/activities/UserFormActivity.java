package fr.efrei.projectlowerhigher.activities;

import static fr.efrei.projectlowerhigher.Constants.USER_PROFILE_PICTURE_DIRECTORY;
import static fr.efrei.projectlowerhigher.Constants.USER_SHARED_PREFERENCES;
import static fr.efrei.projectlowerhigher.Constants.USER_SHARED_PREFERENCES_USER_LOCATION;
import static fr.efrei.projectlowerhigher.Constants.USER_SHARED_PREFERENCES_USER_NAME;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.MediaStore;

import androidx.annotation.RequiresPermission;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import fr.efrei.projectlowerhigher.R;
import fr.efrei.projectlowerhigher.models.UserModel;
import fr.efrei.projectlowerhigher.models.WorldCityModel;
import fr.efrei.projectlowerhigher.repositories.WorldCitiesRepository;
import fr.efrei.projectlowerhigher.repositories.WorldCitiesRepositoryRamImpl;

public class UserFormActivity extends AppCompatActivity {

    private static final String USER_NAME_REGEX = "^(?!.* {3})(?=\\S)(?=.*\\S$)[a-zA-Z0-9-_ ]{3," +
            "100}$";

    private Button photoButton;
    private Button saveButton;
    private Button cancelButton;
    private Button locationButton;
    private ImageView imageView;
    private EditText playerNameEditText;
    private TextView locationText;

    private Bitmap profilePicture = null;

    private ArrayList<WorldCityModel>  worldCityModelList;

    private UserModel user;
    SharedPreferences userPreferences;

    private LocationManager locationManager;

    private static final int PIC_ID = 8277;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_form);
        initializeWidgets();
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        userPreferences = getSharedPreferences(USER_SHARED_PREFERENCES, 0);
        WorldCitiesRepository worldCitiesRepository = new WorldCitiesRepositoryRamImpl(this);
        worldCityModelList = new ArrayList<>(worldCitiesRepository.getAllWorldCities());

        String fetchedUserName = userPreferences.getString(USER_SHARED_PREFERENCES_USER_NAME, null);

        // Initializes current user.
        user = new UserModel();
        user.setName(userPreferences.getString(USER_SHARED_PREFERENCES_USER_NAME, ""));
        playerNameEditText.setText(user.getName());
        user.setLocation(userPreferences.getString(USER_SHARED_PREFERENCES_USER_LOCATION, "Paris"));
        locationText.setText(user.getLocation());
        ContextWrapper contextWrapper = new ContextWrapper(getApplicationContext());
        File directory = contextWrapper.getDir(USER_PROFILE_PICTURE_DIRECTORY,
                Context.MODE_PRIVATE);
        File file = new File(directory, user.getName() + ".bmp");
        if (file.exists()) {
            profilePicture = ((BitmapDrawable) Drawable.createFromPath(file.toString())).getBitmap();
            imageView.setImageBitmap(profilePicture);
        }
    }

    private void initializeWidgets() {
        playerNameEditText = findViewById(R.id.UserFormActivity_nameEditText);

        photoButton = findViewById(R.id.UserFormActivity_photoButton);
        photoButton.setOnClickListener(this::onPhotoButtonPressed);

        saveButton = findViewById(R.id.UserFormActivity_saveButton);
        saveButton.setOnClickListener(this::onSaveButtonPressed);

        cancelButton = findViewById(R.id.UserFormActivity_cancelButton);
        cancelButton.setOnClickListener(view -> finish());

        locationButton = findViewById(R.id.UserFormActivity_locationButton);
        locationButton.setOnClickListener(this::onLocationButtonPressed);

        imageView = findViewById(R.id.UserFormActivity_photoImageView);

        locationText = findViewById(R.id.UserFormActivity_locationText);
    }

    private void onPhotoButtonPressed(View view) {
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(cameraIntent, PIC_ID);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Match the request 'pic id with requestCode
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PIC_ID) {
            profilePicture = (Bitmap) data.getExtras().get("data");
            imageView.setImageBitmap(profilePicture);
        }
    }

    private void onSaveButtonPressed(View view) {
        if (!playerNameEditText.getText().toString().equals("") &&
                profilePicture != null && profilePicture.getByteCount() > 0) {
            user.setName(playerNameEditText.getText().toString());
            try {
                ContextWrapper contextWrapper = new ContextWrapper(getApplicationContext());
                File directory = contextWrapper.getDir(USER_PROFILE_PICTURE_DIRECTORY,
                        Context.MODE_PRIVATE);
                File file = new File(directory, user.getName() + ".bmp");
                FileOutputStream fileOutputStream = new FileOutputStream(file);
                profilePicture.compress(Bitmap.CompressFormat.JPEG, 100, fileOutputStream);
                fileOutputStream.flush();
                fileOutputStream.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                Log.e("GGERGHE", e.getMessage());
            }
            SharedPreferences.Editor sharedPreferenceEditor = userPreferences.edit();
            sharedPreferenceEditor.putString(USER_SHARED_PREFERENCES_USER_NAME, user.getName());
            sharedPreferenceEditor.putString(USER_SHARED_PREFERENCES_USER_LOCATION,
                    user.getLocation());
            sharedPreferenceEditor.apply();
            finish();
        }
    }

    @RequiresPermission(Manifest.permission.ACCESS_COARSE_LOCATION)
    private void onLocationButtonPressed(View view) {
        if (checkAndRequestPermissions()) {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0,
                    new LocationListener() {
                        @RequiresPermission(Manifest.permission.ACCESS_COARSE_LOCATION)
                        public void onLocationChanged(Location location) {
                            Log.i("Location Changed", location.toString());
                            if (location != null) {
                                Log.i("Location Changed",
                                        location.getLatitude() + " and " + location.getLongitude());
                                Double lat = location.getLatitude();
                                Double lng = location.getLongitude();
                                Double minDistance = Double.MAX_VALUE;
                                String closestCityName = "";
                                for (WorldCityModel city : worldCityModelList) {
                                    Double currentDistance = haversine(lat, lng,
                                            city.getLatitude(), city.getLongitude());
                                    if (currentDistance < minDistance) {
                                        minDistance = currentDistance;
                                        closestCityName = city.getName();
                                    }
                                }
                                user.setLocation(closestCityName);
                                locationText.setText(closestCityName);
                                locationManager.removeUpdates(this);
                            }
                        }
                    });
        }
    }

    public boolean checkAndRequestPermissions() {
        int internet = ContextCompat.checkSelfPermission(UserFormActivity.this,
                Manifest.permission.INTERNET);
        int loc = ContextCompat.checkSelfPermission(UserFormActivity.this,
                Manifest.permission.ACCESS_COARSE_LOCATION);
        int loc2 = ContextCompat.checkSelfPermission(UserFormActivity.this,
                Manifest.permission.ACCESS_FINE_LOCATION);
        List<String> listPermissionsNeeded = new ArrayList<>();

        if (internet != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.INTERNET);
        }
        if (loc != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.ACCESS_COARSE_LOCATION);
        }
        if (loc2 != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.ACCESS_FINE_LOCATION);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions((Activity) UserFormActivity.this,
                    listPermissionsNeeded.toArray
                            (new String[listPermissionsNeeded.size()]), 1);
            return false;
        }
        return true;
    }

    double haversine(double lat1, double lon1, double lat2, double lon2) {
        // distance between latitudes and longitudes
        double dLat = Math.toRadians(lat2 - lat1);
        double dLon = Math.toRadians(lon2 - lon1);

        // convert to radians
        lat1 = Math.toRadians(lat1);
        lat2 = Math.toRadians(lat2);

        // apply formulae
        double a = Math.pow(Math.sin(dLat / 2), 2) +
                Math.pow(Math.sin(dLon / 2), 2) *
                        Math.cos(lat1) *
                        Math.cos(lat2);
        double rad = 6371;
        double c = 2 * Math.asin(Math.sqrt(a));
        return rad * c;
    }
}